package ru.ermakov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void updateTask(
            @WebParam(name = "task") TaskDTO task
    ) throws Exception;

    @WebMethod
    TaskDTO findById(
            @WebParam(name = "taskId") String taskId
    ) throws Exception;

    @WebMethod
    TaskDTO findByName(
            @WebParam(name = "taskName") String taskName
    ) throws Exception;

    @WebMethod
    List<TaskDTO> findAllProject() throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "taskId") String taskId
    ) throws Exception;

    @WebMethod
    void removeAllTask() throws Exception;
}
