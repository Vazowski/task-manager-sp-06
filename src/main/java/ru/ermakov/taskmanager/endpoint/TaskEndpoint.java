package ru.ermakov.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ermakov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "task") @Nullable final TaskDTO task
    ) throws Exception {
        taskService.update(task);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findById(
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception{
        return taskService.findById(taskId);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findByName(
            @WebParam(name = "taskName") @Nullable final String taskName
    ) throws Exception{
        return taskService.findByName(taskName);
    }

    @Override
    @Nullable
    @WebMethod
    public List<TaskDTO> findAllProject() throws Exception{
        return taskService.findAll();
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception{
        if (taskId == null)
            return;
        taskService.remove(taskId);
    }

    @Override
    @WebMethod
    public void removeAllTask() throws Exception{
        taskService.removeAll();
    }
}
