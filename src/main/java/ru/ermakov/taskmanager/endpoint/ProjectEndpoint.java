package ru.ermakov.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ermakov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "project") @Nullable final ProjectDTO project
    ) throws Exception {
        projectService.update(project);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findById(
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception{
        return projectService.findById(projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findByName(
            @WebParam(name = "projectName") @Nullable final String projectName
    ) throws Exception{
        return projectService.findByName(projectName);
    }

    @Override
    @Nullable
    @WebMethod
    public List<ProjectDTO> findAllProject() throws Exception{
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception{
        if (projectId == null)
            return;
        projectService.remove(projectId);
    }

    @Override
    @WebMethod
    public void removeAllProject() throws Exception{
        projectService.removeAll();
    }
}