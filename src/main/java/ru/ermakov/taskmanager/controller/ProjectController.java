package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.dto.ProjectDTO;

import javax.faces.bean.ManagedBean;
import java.util.List;

@Component
@ManagedBean(eager = true)
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Nullable
    private ProjectDTO currentProject = new ProjectDTO();

    public void onload(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            return;
        }
        currentProject = projectService.findById(id);
    }

    @Nullable
    public ProjectDTO getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(@Nullable final ProjectDTO currentProject) {
        this.currentProject = currentProject;
    }

    @Nullable
    public ProjectDTO project(@Nullable final String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectService.findById(id);
    }

    @Nullable
    public List<ProjectDTO> list() {
        return projectService.findAll();
    }

    @Nullable
    public String save(@Nullable final ProjectDTO project) {
        projectService.update(project);
        currentProject = null;
        return "pretty:projectList";
    }

    public void delete(@Nullable final ProjectDTO project) {
        if (project == null)
            return;
        projectService.remove(project.getId());
    }

    @Nullable
    public String clear() {
        projectService.removeAll();
        return "pretty:projectList";
    }

    public ReadinessStatus[] getReadinessStatuses() {
        return ReadinessStatus.values();
    }
}