package ru.ermakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ermakov.taskmanager.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @Query(value = "SELECT a FROM Task a WHERE a.name=:name")
    Task findByName(@Param ("name") @NotNull final String name);

    @Nullable
    @Query(value = "SELECT a FROM Task a WHERE a.project.id=:projectId")
    Task findByProjectId(@Param ("projectId") @NotNull final String projectId);
}