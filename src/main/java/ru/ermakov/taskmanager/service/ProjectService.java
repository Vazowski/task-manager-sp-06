package ru.ermakov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.dto.ProjectDTO;
import ru.ermakov.taskmanager.model.Project;
import ru.ermakov.taskmanager.model.Task;
import ru.ermakov.taskmanager.repository.ProjectRepository;
import ru.ermakov.taskmanager.repository.TaskRepository;
import ru.ermakov.taskmanager.util.ProjectDTOConvertUtil;

import java.util.List;

@Service
@Transactional
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;

    public ProjectService() {
    }

    public void update(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null)
            return;
        projectRepository.save(ProjectDTOConvertUtil.DTOToProject(projectDTO));
    }

    @Nullable
    public ProjectDTO findById(@Nullable final String id) {
        if (id.isEmpty())
            return null;
        return ProjectDTOConvertUtil.projectToDTO(projectRepository.getOne(id));
    }

    @Nullable
    public ProjectDTO findByName(@Nullable final String name) {
        if (name.isEmpty())
            return null;
        return ProjectDTOConvertUtil.projectToDTO(projectRepository.findByName(name));
    }

    @Nullable
    public List<ProjectDTO> findAll() {
        return ProjectDTOConvertUtil.projectsToDTO(projectRepository.findAll());
    }

    public void remove(@NotNull final String id) {
        if (id.isEmpty())
            return;
        Project tempProject = projectRepository.getOne(id);
        for (Task task: tempProject.getTaskList()) {
            taskRepository.delete(task);
        }
        projectRepository.delete(tempProject);
    }

    public void removeAll() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
    }
}
