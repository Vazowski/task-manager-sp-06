package ru.ermakov.taskmanager.config;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@PropertySource(value = "classpath:db.properties")
@ComponentScan(basePackages = "ru.ermakov.taskmanager")
@EnableJpaRepositories("ru.ermakov.taskmanager.repository")
public class WebConfig {

    @Bean
    ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    public DataSource dataSource(
            @Value("${db.driver}") final String dataSourceDriver,
            @Value("${db.url}") final String dataSourceUrl,
            @Value("${db.user}") final String dataSourceUser,
            @Value("${db.password}") final String dataSourcePassword
    ) {
        @Nullable
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${db.show_sql}") final boolean showSql,
            @Value("${db.hbm2ddl}") final String tableStrategy,
            @Value("${db.dialect}") final String dialect,
            @Value("${hazel.use_second_level_cache}") final boolean secondLvlCache,
            @Value("${hazel.use_query_cache}") final boolean queryCache,
            @Value("${hazel.use_minimal_puts}") final boolean minimalPuts,
            @Value("${hazel.use_lite_member}") final boolean liteMember,
            @Value("${hazel.region_prefix}") final String regionPrefix,
            @Value("${hazel.conf_file}") final String confFile,
            @Value("${hazel.factory_class}") final String factoryClass
    ) {
        @Nullable
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.ermakov.taskmanager");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);

//        properties.put("hibernate.cache.use_second_level_cache", secondLvlCache);
//        properties.put("hibernate.cache.use_query_cache", queryCache);
//        properties.put("hibernate.cache.use_minimal_puts", minimalPuts);
//        properties.put("hibernate.cache.hazelcast.use_lite_member", liteMember);
//        properties.put("hibernate.cache.region_prefix", regionPrefix);
//        properties.put("hibernate.cache.provider_configuration_file_resource_path", confFile);
//        properties.put("hibernate.cache.region.factory_class", factoryClass);

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            final LocalContainerEntityManagerFactoryBean emf
    ) {
        @Nullable
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }
}