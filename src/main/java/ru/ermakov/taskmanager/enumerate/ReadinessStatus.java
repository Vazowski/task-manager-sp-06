package ru.ermakov.taskmanager.enumerate;


import org.jetbrains.annotations.Nullable;

public enum ReadinessStatus {

    PLANNED("Planned"),
    DURING("During"),
    READY("Ready");

    @Nullable
    private String displayName;

    ReadinessStatus(final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    @Override
    public String toString() {
        return displayName;
    }


    public String getDisplayName() {
        return displayName;
    }
}